package ejercicio_tiempo;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

class Tiempo {
	private Date tiempo;
	private Locale idioma;
	
	public Tiempo() {
		this.tiempo = new Date();
		this.idioma = Locale.forLanguageTag("es-ES");
	}
	
	String fechaFormatoLargo(){
		SimpleDateFormat formato = new SimpleDateFormat("d 'de' MMMM 'de' yyyy", this.idioma);
		String fechaFormateada = formato.format(this.tiempo);
		return fechaFormateada;	
	}
	
	String fechaFormatoCorto(){
		SimpleDateFormat formato = new SimpleDateFormat("dd'/'MM'/'yyy" , this.idioma);
		String fechaFormateada = formato.format(this.tiempo);
		return fechaFormateada;
	}
	
	String hora() {
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm", this.idioma);
		String fechaFormateada = formato.format(this.tiempo);
		return fechaFormateada;
	
	}
}