package ejercicio_tiempo;

import java.util.Date;
import java.util.Locale;

record TiempoRecord(Date tiempo, Locale idioma) {
}
