package ejercicio_tiempo;

import java.util.Date;
import java.util.Locale;

class TiempoAnemico {
	private Date tiempo;
	private Locale idioma;
	
	TiempoAnemico() {
		this.tiempo = new Date();
		this.idioma = Locale.forLanguageTag("es-ES");
	}

	Date getTiempo() {
		return tiempo;
	}

	Locale getIdioma() {
		return idioma;
	}
}
