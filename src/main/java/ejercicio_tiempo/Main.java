package ejercicio_tiempo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
    //usando clase no anémica
    System.out.println("con clase no anémica\n");
    Tiempo tiempo = new Tiempo();  
   	System.out.println(tiempo.hora( ) + " " + tiempo.fechaFormatoLargo());
   	System.out.println(tiempo.hora( ) + " " + tiempo.fechaFormatoCorto());
   	//usando clase anémica
   	System.out.println("\ncon clase anémica\n");
   	TiempoAnemico tiempoAnemico = new TiempoAnemico();
   	SimpleDateFormat formatoLargo = new SimpleDateFormat("d 'de' MMMM 'de' yyyy", tiempoAnemico.getIdioma());
	String fechaFormateadaLargo = formatoLargo.format(tiempoAnemico.getTiempo());
	SimpleDateFormat formatoCorto = new SimpleDateFormat("dd'/'MM'/'yyy", tiempoAnemico.getIdioma());
	String fechaFormateadaCorto = formatoCorto.format(tiempoAnemico.getTiempo());
	SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm", tiempoAnemico.getIdioma());
	String horaFormateada = formatoHora.format(tiempoAnemico.getTiempo());
   	System.out.println(horaFormateada + " " + fechaFormateadaLargo);
   	System.out.println(horaFormateada + " " + fechaFormateadaCorto);
   	//usando clase record
   	System.out.println("\ncon clase record\n");
   	Date dateRecord = new Date();
   	Locale localeRecord = Locale.forLanguageTag("es-ES");
   	TiempoRecord tiempoRecord = new TiempoRecord(dateRecord, localeRecord);
   	SimpleDateFormat formatoLargoR = new SimpleDateFormat("d 'de' MMMM 'de' yyyy", tiempoRecord.idioma());
   	String fechaFormateadaLargoR = formatoLargo.format(tiempoRecord.tiempo());
   	SimpleDateFormat formatoCortoR = new SimpleDateFormat("dd'/'MM'/'yyy", tiempoRecord.idioma());
   	String fechaFormateadaCortoR = formatoCorto.format(tiempoRecord.tiempo());
	SimpleDateFormat formatoHoraR = new SimpleDateFormat("HH:mm", tiempoRecord.idioma());
	String horaFormateadaR = formatoHora.format(tiempoRecord.tiempo());
	System.out.println(horaFormateadaR + " " + fechaFormateadaLargoR);
   	System.out.println(horaFormateadaR + " " + fechaFormateadaCortoR);
	
    }
}
